Для меня выбор был между 3 языками Haskell, Clojure и Erlang. Вакансий, в которых они указываются как дополнительные или основные знания, больше, чем с другими функциональными языками.
 
 Честно говоря, я не могу воспринимать эти языки чем-то большим чем некий общеразвивающий курс, ввиду довольно узким практическим применениям (судя по крупным проектам и отношения к ним в среде разработчиков: все говорят, что это классно и сколько всего можно сделать, но почему-то никто ничего не делает). В этом плане я бы выбрал Scala или Rust (но сомневаюсь, что смогу идти по ним путем чистой функциональщины).
 
Из 3х первоначальных языков я бы выбрал Clojure (на самом деле все 3 пока темны для меня, но Clojure транслируется в байт код и это кажется довольно интересным, особенно), так же, довольно удобно, что он поддерживает java библиотеки(хотя в процессе выполнения лабараторных работ, скорее всего, мне это не пригодится ). Прошерстив несколько форумов в отношении Haskell я обнаружил(в основном) именно такие мнения:
 
 "Haskell is fantastic and learning it changed how I think about a lot of software problems in ways that made me a better programmer in general ... I didn't end up making enough useful things with it... part of it was that the web libraries were weak, they had interesting and useful features but were mostly solving problems in the wrong way or solving the wrong problems"
 
"Clojure had equally strong changes to how I think about programming by finally showing me why I want real macros and lispy syntax.
The web focused stuff is clearly written by people who have worked in that environment for a long time and know what the important problems are and what kinds of libraries will be most useful.
On top of that the Clojure community has this ridiculous blend of academic smarts and pragmatic discipline."
 
В отношении Erlang, после недолгого изучения я решил повременить, т.к. пока что он кажется мне слишком обьемным и материалов по нему(в сравнении с Clojure) я нашел меньше.
 
Если я правильно понял, для разработки на Clojure мне понадобится сам язык и среда разработки REPL, в которой можно будет разрабатывать программу интерактивно (раньше я так не делал)

Язык: Clojure 1.11.1
 
Lint: Clj-kondo (https://github.com/clj-kondo/clj-kondo)
 
Tool for formatting Clojure code: cljfmt (https://github.com/weavejester/cljfmt)
 
Сборка: Leiningen (https://leiningen.org/)
 
Тестирование: clojure.test (https://clojure.github.io/clojure/clojure.test-api.html)
 
Стиль: https://guide.clojure.style/
 
Книжка: https://www.braveclojure.com/ (обложка красивая)
